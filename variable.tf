variable "cidr_block_vpc" {
type = string
description = "cidr for vpc"
default = "162.58.0.0/17"
}

variable "vpc_tags"{
type = any
default = {Name = "tool_vpc"}
description = "tags for vpc"
}
