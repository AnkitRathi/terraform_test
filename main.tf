resource "aws_vpc" "vpc" {
  cidr_block = var.cidr_block_vpc
  tags       = var.vpc_tags
}
